import sqlite3

class SQLITE():
    def __init__(self,tableName):
        self.tableName = tableName
        self.connect = sqlite3.connect(tableName)
        self.cursor = self.connect.cursor()

    def create(self):
        try:
            self.cursor.execute("CREATE TABLE tableName (palabra text,definicion text)",{'tableName':self.tableName})
            print('La tabla se creado exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)

    def post(self,palabra,definicion):
        try:
            self.cursor.execute("INSERT INTO tableName VALUES (:palabra,:definicion)",{'tableName':self.tableName,'palabra':palabra,'definicion':definicion})
            self.connect.commit()
            print('La palabra se añadido exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)
            
    
    def updatePalabra(self,palabra,newPalabra):
        try:
            self.cursor.execute("UPDATE  tableName SET palabra=:newPalabra WHERE palabra=:palabra",{'tableName':self.tableName,'palabra':palabra,'newPalabra':newPalabra})
            self.connect.commit()
            print('La palabra se actualizado exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)
            
    
    def updateDefinicion(self,palabra,definicion):
        try:
            self.cursor.execute("UPDATE  tableName SET definicion=:definicion WHERE palabra=:palabra",{'tableName':self.tableName,'palabra':palabra,'definicion':definicion})
            self.connect.commit()
            print('La definicion se actualizado exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)
            
    
    def delete(self,palabra):
        try:
            self.cursor.execute("DELETE FROM tableName WHERE palabra=:palabra",{'tableName':self.tableName,'palabra':palabra})
            self.connect.commit()
            print('La palabra se eliminado exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)
            
    
    def getAllPalabras(self):
        try:
            self.cursor.execute("SELECT palabra from tableName",{'tableName':self.tableName})
            print(self.cursor.fetchall())
        except Exception as err:
            print('algo salio mal:')
            print(err)
            
    
    def getDefinicion(self,palabra):
        try:
            self.cursor.execute("SELECT definicion from tableName WHERE palabra=:palabra",{'tableName':self.tableName,'palabra':palabra})
            print(self.cursor.fetchone())
        except Exception as err:
            print('algo salio mal:')
            print(err)
            
    
    def close(self):
        self.connect.close()

def main(nombreTabla):
    sql = SQLITE(nombreTabla)
    exit = False
    op = input('1-CrearTAbla  2-Agregar palabra  \n3-Actualizar palabra  4-Actualizar definicion  \n5-Lista de palabras  6-Buscar difinicion  \n7-Eliminar palabra   8-SALIR 🏃‍♂️ \n')
    if(str(op) == '1'):
        print('⚠  RES:')
        sql.create()
        
    elif(str(op) == '2'):
        print('⚠  RES:')
        sql.post(input('introduzca la palabra:'),input('introduzca su definicion:'))
        
    elif(str(op) == '3'):
        print('⚠  RES:')
        sql.updatePalabra(input('introduzca la palabra que quiera actualizar:'),input('introduzca la nueva palabra:'))
        
    elif(str(op) == '4'):
        print('⚠  RES:')
        sql.updateDefinicion(input('introduzca la palabra que quiera actualizar su definicion:'),input('introduzca la nueva definicion:'))
        
    elif(str(op) == '5'):
        print('⚠  RES:')
        sql.getAllPalabras()
        
    elif(str(op) == '6'):
        print('⚠  RES:')
        sql.getDefinicion(input('introduzca la palabra que quiera buscar la definicion:'))
        
    elif(str(op) == '7'):
        print('⚠  RES:hay veces en la que debe salir para que se apliquen los cambios')
        sql.delete(input('introduzca la palabra que quieras borrar:'))
    
    elif(str(op) == '8'):
        exit = True
    if(not exit):
        main(nombreTabla)
    sql.close()
    print('⚠  RES:Saliendo')
    


if __name__ == "__main__":
    print('\nBIENCENIDO AL DICCIONARIO DE SLANG PANAMEÑO ✨\n')
    print('nota:"el nombre de la tabla puede ser palabras que ya tiene algunas palabras"')
    main(input('Itroduzca nombre de la tabla:'))
